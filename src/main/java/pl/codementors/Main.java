package pl.codementors;

import pl.codementors.Builder.MachineBuilder;
import pl.codementors.Models.FactoryLoadFromFile;
import pl.codementors.Models.FactorySaveToFile;
import pl.codementors.Models.Machine;
import pl.codementors.Models.MachineService;
import pl.codementors.Models.SimpleRepository;
import pl.codementors.Models.SimpleRepositoryAdapter;
import pl.codementors.Models.SimpleStorage;
import pl.codementors.Models.Size;
import java.util.Scanner;

import static pl.codementors.Enum.MachineType.EXTENDED;
import static pl.codementors.Enum.MachineType.STANDARD;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("MASZYNY");
        System.out.println();

        SimpleStorage storage = new SimpleStorage();
        SimpleRepository simpleRepository = new SimpleRepository(storage);
        SimpleRepositoryAdapter simpleRepositoryAdapter = new SimpleRepositoryAdapter(simpleRepository);
        MachineService machineService = new MachineService(simpleRepositoryAdapter);

        boolean running = true;

        while(running){

            System.out.println("Menu: " +
                    "\n1 - dodaj maszynę" +
                    "\n2 - usuń maszynę" +
                    "\n3 - wyświetl wszystkie maszyny" +
                    "\n4 - wyszukaj po typie" +
                    "\n5 - wyszukaj po nazwie" +
                    "\n6 - wczytaj maszyny z pliku" +
                    "\n7 - zapisz maszyny do pliku" +
                    "\n8 - koniec");

            String number = scanner.next();

            switch (number){
                case "1":{
                    MachineBuilder mBuilder = new MachineBuilder();

                    System.out.println("Podaj imię");
                    String name = scanner.next();
                    System.out.println("Podaj typ maszyny: " +
                            "\nS - STANDARD" +
                            "\nE - EXTENDED");
                    String type = scanner.next();

                        if(type.equalsIgnoreCase("S")){
                            mBuilder.withType(STANDARD);
                        }
                        else if(type.equalsIgnoreCase("E")){
                            mBuilder.withType(EXTENDED);
                        }

                    System.out.println("Podaj rozmiar");
                    int size = scanner.nextInt();
                    System.out.println("Podaj wysokość");
                    int height = scanner.nextInt();

                    mBuilder.withName(name);
                    mBuilder.withSize(new Size(size, height));
                    Machine machine = mBuilder.build();
                    machineService.addIfNotExists(machine);
                    System.out.println("Dodano maszynę: " + machine);
                    break;
                }
                case "2":{
                    System.out.println("Kasowanie po:" +
                            "\nT - typie" +
                            "\nI - po imieniu");
                    String choice = scanner.next();

                        if(choice.equalsIgnoreCase("T")){
                            System.out.println("Wybierz typ:" +
                                    "\nS - STANDARD" +
                                    "\nE - EXTENDED");
                            String machineType = scanner.next();
                            if(machineType.equalsIgnoreCase("S")){
                                machineService.deleteByType(STANDARD);
                            }else if(machineType.equalsIgnoreCase("E")){
                                machineService.deleteByType(EXTENDED);
                            }
                            System.out.println("Usunięto maszynę");
                        }else if(choice.equalsIgnoreCase("I")){
                            System.out.println("Wpisz imię");
                            String name = scanner.next();
                            machineService.deleteByName(name);
                            System.out.println("Usunięto maszynę");
                        }
                    break;
                }
                case "3":{
                    System.out.println("Wypisuje wszystkie maszyny");
                    System.out.println(machineService.findAll());
                    break;
                }
                case "4":{
                    System.out.println("Wyszukaj maszynę po typie:" +
                            "\nS - STANDARD" +
                            "\nE - EXTENDED");
                    String machineType = scanner.next();
                        if(machineType.equalsIgnoreCase("S")){
                            System.out.println("Maszyny z typem: STANDARD");
                            System.out.println(machineService.findWithType(STANDARD));
                        }else if(machineType.equalsIgnoreCase("E")){
                            System.out.println("Maszyny z typem: EXTENDED");
                            System.out.println(machineService.findWithType(EXTENDED));
                        }
                    break;
                }
                case "5":{
                    System.out.println("Podaj nazwę maszyny do wyszukania");
                    String machineName = scanner.next();
                    System.out.println(machineService.findByName(machineName));
                    break;
                }
                case "6":{
                    System.out.println("Wczytano maszyny z pliku");
                    FactoryLoadFromFile loadFromFile = new FactoryLoadFromFile();
                    loadFromFile.get().loadMachines(machineService, "m.txt");
                    break;
                }
                case "7":{
                    System.out.println("Zapisano maszyny do pliku");
                    FactorySaveToFile saveWithComa = new FactorySaveToFile();
                    saveWithComa.get().saveMachines(machineService,"m.txt");
                    break;
                }
                case "8":{
                    System.out.println("BYE");
                    running = false;
                    break;
                }
            }
        }
    }
}
