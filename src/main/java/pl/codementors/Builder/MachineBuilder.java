package pl.codementors.Builder;

import pl.codementors.Enum.MachineType;
import pl.codementors.Models.Machine;
import pl.codementors.Models.Size;

public class MachineBuilder {
    private String name;
    private MachineType type;
    private Size size;

    public MachineBuilder withName (String name){
        this.name = name;
        return this;
    }

    public MachineBuilder withType (MachineType type){
        this.type = type;
        return this;
    }

    public MachineBuilder withSize (Size size){
        this.size = size;
        return this;
    }

    public Machine build(){
        Machine m = new Machine();
        m.setName(this.name);
        m.setSize(this.size);
        m.setType(this.type);
        return m;
    }
}
