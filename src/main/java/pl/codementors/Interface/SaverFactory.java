package pl.codementors.Interface;

public interface SaverFactory {
    Saver get();
}
