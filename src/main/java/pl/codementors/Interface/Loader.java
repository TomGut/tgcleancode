package pl.codementors.Interface;

import pl.codementors.Models.MachineService;

public interface Loader {
    void loadMachines(MachineService machineService, String file);
}
