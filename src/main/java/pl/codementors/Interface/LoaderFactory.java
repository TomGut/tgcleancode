package pl.codementors.Interface;

public interface LoaderFactory {
    Loader get();
}
