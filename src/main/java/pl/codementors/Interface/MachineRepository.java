package pl.codementors.Interface;

import pl.codementors.Models.Machine;
import pl.codementors.Enum.MachineType;

import java.util.List;

public interface MachineRepository {
    void add(Machine machine);
    boolean remove(Machine machine);
    List<Machine> getAll();
    List<Machine> findBy(MachineType machineType);
    List<Machine> findByName(String machineName);
}
