package pl.codementors.Interface;

import pl.codementors.Models.Machine;

import java.util.List;

public interface Storage {
    void add(Machine machine) throws RuntimeException;
    boolean delete(Machine machine);
    List<Machine> allMachines();
}