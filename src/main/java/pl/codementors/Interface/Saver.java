package pl.codementors.Interface;

import pl.codementors.Models.MachineService;

public interface Saver {
    void saveMachines(MachineService machineService, String file);
}
