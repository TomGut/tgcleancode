package pl.codementors.Models;

import pl.codementors.Interface.Storage;

import java.util.ArrayList;
import java.util.List;

public class SimpleStorage implements Storage {
    private List<Machine> dataStorage = new ArrayList<>();

    @Override
    public void add(Machine machine) throws RuntimeException {
        if(machine == null){
            throw new RuntimeException();
        }else{
            dataStorage.add(machine);
        }
    }

    @Override
    public boolean delete(Machine machine) {

        if(dataStorage.isEmpty()){
            return false;
        }else{
            dataStorage.remove(machine);
        }
        return true;
    }

    @Override
    public List<Machine> allMachines() {
        List<Machine> dataStorageCopy = new ArrayList<>();
        dataStorageCopy.addAll(dataStorage);
        return dataStorageCopy;
    }

    @Override
    public String toString() {
        return "SimpleStorage{" + "dataStorage=" + dataStorage + '}';
    }
}
