package pl.codementors.Models;

import pl.codementors.Interface.Loader;
import pl.codementors.Interface.LoaderFactory;

public class FactoryLoadFromFile implements LoaderFactory {

    @Override
    public Loader get() {
        return new LoadWithComa();
    }
}
