package pl.codementors.Models;

public class Size {
    private int size;
    private int height;

    public Size(int size, int height) {
        this.size = size;
        this.height = height;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
