package pl.codementors.Models;

import pl.codementors.Enum.MachineType;
import pl.codementors.Interface.MachineRepository;

import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepositoryAdapter implements MachineRepository {

    private SimpleRepository repository;

    public SimpleRepositoryAdapter(SimpleRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(Machine machine) {
        repository.add(machine);
    }

    @Override
    public boolean remove(Machine machine) {
        if(repository
                .getStorage()
                .allMachines()
                .isEmpty() ||
                repository
                        .getStorage()
                        .allMachines()
                        .size() == 0){
            return false;
        }
        repository.remove(machine);
        return true;
    }

    @Override
    public List<Machine> getAll() {
        List<Machine> machines = repository
                .getStorage()
                .allMachines();
        return machines;
    }

    @Override
    public List<Machine> findBy(MachineType machineType) {
        List<Machine> byType = repository
                .getStorage()
                .allMachines()
                .stream()
                .filter(machine -> {
                    if(machine.getType() == machineType){
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList());
        return byType;
    }

    @Override
    public List<Machine> findByName(String machineName) {
        List<Machine> byName = repository
                .getStorage()
                .allMachines()
                .stream()
                .filter(machine -> {
                    if(machine.getName() == machineName){
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList());
        return byName;
    }
}
