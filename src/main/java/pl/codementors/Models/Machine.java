package pl.codementors.Models;

import pl.codementors.Enum.MachineType;

public class Machine {
    private String name;
    private MachineType type;
    private Size size;

    public Machine(){

    }

    public Machine(String name, MachineType type, Size size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MachineType getType() {
        return type;
    }

    public void setType(MachineType type) {
        this.type = type;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Machine machine = (Machine) o;

        if (name != null ? !name.equals(machine.name) : machine.name != null) return false;
        if (type != machine.type) return false;
        return size != null ? size.equals(machine.size) : machine.size == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name + "\n" + type + "\n" + size.getHeight() + "\n" + size.getSize() +"\n";
    }
}
