package pl.codementors.Models;

import pl.codementors.Enum.MachineType;
import pl.codementors.Interface.Loader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LoadWithComa implements Loader {
    @Override
    public void loadMachines(MachineService machineService, String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while (br.ready()) {
                String name = br.readLine();
                String type = br.readLine();
                String height = br.readLine();
                String size = br.readLine();
                br.readLine();
                MachineType mType = MachineType.valueOf(type);
                int sizeInt = Integer.parseInt(size);
                int heightInt = Integer.parseInt(height);

                Machine m = new Machine(name, mType, new Size(sizeInt, heightInt));
                machineService.addIfNotExists(m);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
