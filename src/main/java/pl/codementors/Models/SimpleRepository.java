package pl.codementors.Models;

import pl.codementors.Enum.MachineType;

import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepository{

    private SimpleStorage storage;

    public SimpleRepository(SimpleStorage storage) {
        this.storage = storage;
    }

    public SimpleStorage getStorage() {
        return storage;
    }

    public void setStorage(SimpleStorage storage) {
        this.storage = storage;
    }

    public void add(Machine machine){
        storage.add(machine);
    }

    public boolean remove(Machine machine){
        if(storage.allMachines().isEmpty()){
            return false;
        }else
        storage.delete(machine);
        return true;
    }

    public List<Machine> find(MachineType machineType){
        List<Machine> findMachine = storage
                .allMachines()
                .stream()
                .filter(machine -> machine.getType() == machineType)
                .collect(Collectors.toList());
        return findMachine;
    }
}
