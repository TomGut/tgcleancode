package pl.codementors.Models;

import pl.codementors.Enum.MachineType;
import pl.codementors.Interface.MachineRepository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class MachineService {

    private MachineRepository repository;

    public MachineService(MachineRepository repository) {
        this.repository = repository;
    }

    public boolean addIfNotExists(Machine machine){
        if(!repository
                .getAll()
                .contains(machine)){
            repository.add(machine);
            return true;
        }
        return false;
    }

    public boolean deleteByName(String machineName){
        List<Machine> machines = repository.getAll();

        for (Machine machine: machines){
            if(machine.getName().equals(machineName)){
                repository.remove(machine);
                return true;
            }
        }
        return false;
    }

    public boolean deleteByType(MachineType machineType){
        List<Machine> machines = repository.findBy(machineType);

        boolean deleted = false;
        for (Machine machine: machines){
            repository.remove(machine);
            deleted = true;
        }
        return  deleted;
    }

    public List<Machine> findByName(String machineName){
        List<Machine> machines = repository
                .getAll()
                .stream()
                .filter(machine -> {
                    return machine.getName().equals(machineName);
                }).collect(Collectors.toList());

        return machines;
    }

    public List<Machine> findWithType(MachineType machineType){
        List<Machine> machines = repository
                .getAll()
                .stream()
                .filter(machine -> {
                    return machine.getType() == machineType;
                }).collect(Collectors.toList());

        return machines;
    }

    public List<Machine> findAll(){
        return repository.getAll();
    }

    /*
    public void saveMachines(String file){
        List<Machine> machines = repository.getAll();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

            for (Machine machine : machines) {
                bw.write(machine.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */

    /*
    public void loadMachines(String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while (br.ready()) {
                String name = br.readLine();
                String type = br.readLine();
                String height = br.readLine();
                String size = br.readLine();
                MachineType mType = MachineType.valueOf(type);
                int sizeInt = Integer.parseInt(size);
                int heightInt = Integer.parseInt(height);

                Machine m = new Machine(name, mType, new Size(sizeInt, heightInt));
                repository.add(m);
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
    */
}