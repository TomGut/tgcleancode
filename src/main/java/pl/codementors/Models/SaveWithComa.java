package pl.codementors.Models;

import pl.codementors.Interface.Saver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class SaveWithComa implements Saver{

    @Override
    public void saveMachines(MachineService machineService, String file) {
        List<Machine> machines = machineService.findAll();

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

            for (Machine machine : machines) {
                bw.write( machine.toString());
                bw.write(",");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
