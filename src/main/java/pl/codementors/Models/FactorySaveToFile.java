package pl.codementors.Models;

import pl.codementors.Interface.Saver;
import pl.codementors.Interface.SaverFactory;

public class FactorySaveToFile implements SaverFactory {
    @Override
    public Saver get() {
        return new SaveWithComa();
    }
}
